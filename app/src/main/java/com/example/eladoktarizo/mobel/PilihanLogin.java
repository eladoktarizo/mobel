package com.example.eladoktarizo.mobel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.eladoktarizo.mobel.app.SharedPrefManager;

import java.util.HashMap;
import java.util.Map;

public class PilihanLogin extends AppCompatActivity {

    private static final String TAG = PilihanLogin.class.getSimpleName();
    Button btnloginortu, btnloginguru;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pilihan_login);

        btnloginguru = findViewById(R.id.buttonlogin1);
        btnloginortu = findViewById(R.id.buttonlogin2);

        //register token fcm device ke database
        String token = SharedPrefManager.getInstance(this).getDeviceToken();
        Log.e("TOKEN", token);
        if (token != null) {
            registerDeviceToken(token);
        }

        btnloginguru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PilihanLogin.this, LoginGuru.class);
                startActivity(intent);
            }
        });

        btnloginortu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PilihanLogin.this, Login.class);
                startActivity(intent);
            }
        });
    }

    private void registerDeviceToken(final String token) {
        String URL_REG_TOKEN = Server.URL + "register_token.php";
        StringRequest request = new StringRequest(Request.Method.POST, URL_REG_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG,"respon reg token" + response);
                if (response.equals("existed")) {
                    Toast.makeText(getApplicationContext(), "Token sudah ada", Toast.LENGTH_LONG).show();
                } else if (response.equals("success")){
                    Toast.makeText(getApplicationContext(), "Register token berhasil", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Register token gagal", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if (error instanceof NetworkError) {
                    Toast.makeText(getApplicationContext(), "Periksa koneksi Anda", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("token", token);
                return params;
            }
        };
        MySingleton.getmInstance(this).addToRequestque(request);
    }
}

package com.example.eladoktarizo.mobel;

import android.util.Log;

import com.example.eladoktarizo.mobel.app.SharedPrefManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Elad Oktarizo on 31/05/2018.
 */

public class FcmInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("REFRESHED TOKEN", refreshedToken);

        //menyimpan token ke shared preference
        SharedPrefManager.getInstance(getApplicationContext()).saveDeviceToken(refreshedToken);
    }
}
